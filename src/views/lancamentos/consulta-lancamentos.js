import React from "react";
import {withRouter} from 'react-router-dom'
import Card from "../../components/card";
import FormGroup from "../../components/form-group";
import SelectMenu from "../../components/selectMenu";
import LancamentosTable from "./lancamentosTable";
import * as mensagens from '../../components/toastr'
import LancamentoService from "../../app/service/lancamentoService";
import LocalStorageService from "../../app/service/localstorageService";
import { Dialog } from 'primereact/dialog';
import { Button } from 'primereact/button';

class ConsultaLancamentos extends React.Component {

    state = {
        ano: '',
        mes: '',
        tipo: '',
        descricao: '',
        lancamentos: [],
        usuario: '',
        showConfirmDialog: false,
        lancamentoDeletar: {}
    }

    constructor(){
        super();
        this.service = new LancamentoService();
    }

    buscar = () => {
        if (!this.state.ano) {
            mensagens.mensagemAlert('O preenchimento do campo Ano é obrigatório.')
            return false;
        }

        const usuario = LocalStorageService.obterItem('_usuario_logado') 
        const lancamentoFiltro = {
            ano: this.state.ano,
            mes: this.state.mes,
            tipo: this.state.tipo,
            descricao: this.state.descricao,
            usuario: usuario.id
        }
        this.service
            .consultar(lancamentoFiltro)
            .then( response => {
                const lista = response.data;
                if(lista.length < 1) {
                    mensagens.mensagemAlert('Nenhum resultado encontrado.');
                }
                this.setState({lancamentos: lista})
            }).catch(erro => {
                mensagens.mensagemErro(erro);
            })
    }

    editarAction = (id) => {
        this.props.history.push(`/cadastro-lancamentos/${id}`);
    }

    abrirConfirmacaoDeletar = (lancamento) => {
        this.setState({showConfirmDialog: true, lancamentoDeletar: lancamento})
    }

    cancelarDelecao = () => {
        this.setState({showConfirmDialog: false, lancamentoDeletar: {}})
    }

    deletarAction = () => {
        this.service
            .deletar(this.state.lancamentoDeletar.id)
            .then(response => {
                const lancamentos = this.state.lancamentos;
                const indexLancamento = lancamentos.indexOf(this.state.lancamentoDeletar);
                lancamentos.splice(indexLancamento, 1);
                this.setState({lancamentos: lancamentos, showConfirmDialog: false, lancamentoDeletar: {}});
                mensagens.mensagemSucesso('Lançamento deletado com sucesso.');
            }).catch(erro => {
                mensagens.mensagemErro(erro.response.data)
            });
    }

    alterarStatus = (lancamento, status) => {
        this.service
            .alterarStatus(lancamento.id, status)
                .then(response => {
                    const lancamentos = this.state.lancamentos;
                    const index = lancamentos.indexOf(lancamento);
                    if (index !== -1) {
                        lancamento['status'] = status;
                        lancamentos[index] = lancamento;
                        this.setState({lancamentos});
                    }
                    mensagens.mensagemSucesso('Status atualizado com sucesso.');
                })
    }

    render() {
        const meses = this.service.obterListaMeses();
        const tipos = this.service.obterListaTipos();

        const confirmDialogFooter = (
            <div>
                <Button label="Confirmar" icon="pi pi-check" onClick={this.deletarAction} />
                <Button label="Cancelar" icon="pi pi-times" onClick={this.cancelarDelecao} />
            </div>
        );

        return (
            <Card title="Consulta Lançamentos">
                <div className="row">
                    <div className="col-md-6">
                        <div className="bs-component">
                            <FormGroup label="Ano: *" htmlFor="inputAno">
                                <input type="text" className="form-control" id="inputAno"
                                    value={this.state.ano}
                                    onChange={e => this.setState({ano: e.target.value})}
                                    placeholder="Digite o ano" />
                            </FormGroup>
                            <FormGroup label="Mês: " htmlFor="inputMes">
                                <SelectMenu id="inputMes" className="form-control" lista={meses} 
                                            value={this.state.mes}
                                            onChange={e => this.setState({mes : e.target.value })}
                                />
                            </FormGroup>
                            <FormGroup label="Descrição: " htmlFor="inputDescricao">
                                <input type="text" className="form-control" id="inputDescricao"
                                        value={this.state.descricao}
                                        onChange={e => this.setState({descricao: e.target.value})}
                                        placeholder="Digite a descrição" />
                            </FormGroup>
                            <FormGroup label="Tipo Lançamento: " htmlFor="inputTipo">
                                <SelectMenu id="inputTipo" className="form-control" lista={tipos}
                                            value={this.state.tipo}
                                            onChange={e => this.setState({tipo : e.target.value })}
                                />
                            </FormGroup>
                            
                            <button onClick={this.buscar} type="button" className="btn btn-success"><i className="pi pi-search"></i>  Buscar</button>
                            <button onClick={e => this.props.history.push('/cadastro-lancamentos')} type="button" className="btn btn-danger"><i className="pi pi-plus"></i> Cadastrar</button>
                        </div>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="col-md-12">
                        <div className="bs-component">
                            <LancamentosTable lancamentos={this.state.lancamentos} deletarAction={this.abrirConfirmacaoDeletar} editarAction={this.editarAction} alterarAction={this.alterarStatus} />
                        </div>
                    </div>
                </div>
                <div>
                <Dialog header="Excluir Lançamento" 
                        visible={this.state.showConfirmDialog} 
                        style={{ width: '50vw' }} 
                        modal={true}
                        footer={confirmDialogFooter}
                        onHide={() => this.setState({showConfirmDialog: false})}>
                    <p>Confirma a exclusão deste lançamento? </p>
                </Dialog>
                </div>
            </Card>
        )
    }

}

export default withRouter(ConsultaLancamentos);