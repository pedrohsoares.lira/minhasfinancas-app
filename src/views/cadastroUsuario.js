import React from "react";
import Card from "../components/card";
import FormGroup from "../components/form-group";
import { withRouter } from 'react-router-dom'
import UsuarioService from "../app/service/usuarioService";
import { mensagemSucesso, mensagemErro } from '../components/toastr'

class CadastroUsuario extends React.Component {

    state = {
        nome : '',
        email : '',
        senha : '',
        confirmarSenha : ''
    }
    constructor() {
        super();
        this.service = new UsuarioService();
    }

    validar() {
       
    }

    cadastrar = () => {
        const {nome,email,senha,confirmarSenha} = this.state;
        const usuario = {nome,email,senha,confirmarSenha};

        try {
            this.service.validar(usuario);
        } catch (error) {
            const erros = error.mensagens;
            erros.forEach(msg => mensagemErro(msg));
            return false;
        }
        
        this.service.salvar(usuario)
            .then(response => {
                mensagemSucesso('Usuário cadastrado com sucesso! Faça o login para acessar o sistema.');
                this.props.history.push("/login");
            }).catch(erro => {
                mensagemErro(erro.response.data);
            })
    }

    cancelar = () => {
        this.props.history.push("/login");
    }

    render() {
        return (
            <Card title="Cadastro de Usuário">
                <div className="row">
                    <div className="col-lg-12">
                        <div className="bs-component">
                            <FormGroup label="Nome: *" htmlFor="inputNome">
                                <input type="text" className="form-control" id="inputNome" name="nome" onChange={e => this.setState({nome: e.target.value})} />
                            </FormGroup>
                            <FormGroup label="Email: *" htmlFor="inputEmail">
                                <input type="email" className="form-control" id="inputEmail" name="email" onChange={e => this.setState({email: e.target.value})} />
                            </FormGroup>
                            <FormGroup label="Senha: *" htmlFor="inputSenha">
                                <input type="password" className="form-control" id="inputSenha" name="senha" onChange={e => this.setState({senha: e.target.value})} />
                            </FormGroup>
                            <FormGroup label="Confirmar Senha: *" htmlFor="inputConfirmarSenha">
                                <input type="password" className="form-control" id="inputConfirmarSenha" name="confirmarSenha" onChange={e => this.setState({confirmarSenha: e.target.value})} />
                            </FormGroup>
                            <button onClick={this.cadastrar} type="button" className="btn btn-success"><i className="pi pi-save"></i>Salvar</button>
                            <button onClick={this.cancelar} type="button" className="btn btn-danger"><i className="pi pi-time"></i>Cancelar</button>
                        </div>
                    </div>
                </div>
            </Card>
        )
    }
}

export default withRouter (CadastroUsuario);