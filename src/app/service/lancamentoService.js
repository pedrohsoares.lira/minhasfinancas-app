import ApiService from "../apiservice";
import ErroValidacao from "../exception/erroValidacao";

class LancamentoService extends ApiService {
    constructor() {
        super('/api/lancamentos');
    }

    salvar(lancamento) {
        return this.post('/', lancamento);
    }

    atualizar(lancamento) {
        return this.put(`/${lancamento.id}`, lancamento);
    }

    obterPorId(id) {
        return this.get(`/${id}`);
    }

    consultar(lancamentoFiltro) {
        let params = `?ano=${lancamentoFiltro.ano}`;

        if (lancamentoFiltro.mes) {
            params = `${params}&mes=${lancamentoFiltro.mes}`;
        }

        if (lancamentoFiltro.tipo) {
            params = `${params}&tipo=${lancamentoFiltro.tipo}`;
        }

        if (lancamentoFiltro.status) {
            params = `${params}&status=${lancamentoFiltro.status}`;
        }

        if (lancamentoFiltro.usuario) {
            params = `${params}&usuario=${lancamentoFiltro.usuario}`;
        }

        if (lancamentoFiltro.descricao) {
            params = `${params}&descricao=${lancamentoFiltro.descricao}`;
        }

        return this.get(params);
    }

    alterarStatus(id, status) {
        return this.put(`/${id}/atualiza-status`, {status});
    }

    deletar(id) {
        return this.delete(`/${id}`);
    }

    validar(lancamento) {
        const erros = [];
        if (!lancamento.ano) {
            erros.push('Informe o Ano.');
        }

        if (!lancamento.mes) {
            erros.push('Informe o Mês.');
        }
        
        if (!lancamento.descricao) {
            erros.push('Informe a Descrição.');
        }
        
        if (!lancamento.valor) {
            erros.push('Informe o Valor.');
        }
        
        if (!lancamento.tipo) {
            erros.push('Informe o Tipo.');
        }




        if (erros && erros.length > 0) {
            throw new ErroValidacao(erros)
        }
    }

    obterListaMeses() {
        return [
            {label: 'SELECIONE...', value: ''},
            {label: 'JANEIRO', value: 1},
            {label: 'FEVEREIRO', value: 2},
            {label: 'MARÇO', value: 3},
            {label: 'ABRIL', value: 4},
            {label: 'MAIO', value: 5},
            {label: 'JUNHO', value: 6},
            {label: 'JULHO', value: 7},
            {label: 'AGOSTO', value: 8},
            {label: 'SETEMBRO', value: 9},
            {label: 'OUTUBRO', value: 10},
            {label: 'NOVEMBRO', value: 11},
            {label: 'DEZEMBRO', value: 12}
        ];
    }

    obterListaTipos() {
        return [
            {label: 'SELECIONE...', value: ''},
            {label: 'RECEITA', value: 'RECEITA'},
            {label: 'DESPESA', value: 'DESPESA'}
        ];
    }
}

export default LancamentoService